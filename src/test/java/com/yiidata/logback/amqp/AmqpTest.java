package com.yiidata.logback.amqp;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Random;

/**
 *
 */
public class AmqpTest {

    private static final String EXCHANGE = "logback";

    String queneName = "testtopic";
    Connection connection = null;
    Channel channel = null;

    @Before
    public void setUp() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setPort(5672);
        factory.setUsername("admin");
        factory.setPassword("123456");
        factory.setVirtualHost("/");
        // 创建与RabbitMQ服务器的TCP连接
        connection = factory.newConnection();
        // 创建一个频道
        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE, BuiltinExchangeType.DIRECT);//   不同类型

        // 声明默认的队列
        //channel.queueDeclare(queneName, true, false, false, null);
    }

    @After
    public void tearDown() throws Exception {
        if(this.connection != null) {
            this.connection.close();
        }
    }

    @org.junit.Test
    public void testSend() throws Exception {
        for (int i = 0; i < 1000000000; i++) {
            Object[] arg = new Object[10];
            arg[0] = i;
            arg[1] = "dsp7ga_" + i;
            arg[2] = new Timestamp(System.currentTimeMillis());
            arg[3] = new Timestamp(System.currentTimeMillis());
            arg[4] = 1;
            arg[5] = "hello primeton dsp.";
            arg[6] = i + new Random().nextFloat();
            arg[7] = new Random().nextDouble();
            arg[8] = new Random().nextInt(100);
            arg[9] = new java.util.Date();

            channel.basicPublish(EXCHANGE, queneName, null, StringUtils.join(arg, ",").getBytes());
            if(i%100 == 0){
                Thread.sleep(new Random().nextInt(10) * 1000);
            }
        }
    }


    @org.junit.Test
    public void testReceiver() throws Exception {
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
                                       byte[] body) throws IOException {
                String message = new String(body, StandardCharsets.UTF_8);
                System.out.println(envelope.getExchange() + "," + envelope.getRoutingKey() + "," + message);
            }
        };
        // channel绑定队列，autoAck为true表示一旦收到消息则自动回复确认消息
        channel.basicConsume(queneName, true, consumer);
        while (true) {
            Thread.sleep(200);
        }
    }


}
