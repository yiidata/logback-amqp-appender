## logback-amqp-appender

> 该项目是 logback 日志发送到 rabbitmq 的实现。SpringBoot 提供了 logback 到 amqp 的实现，但是项目如果非 Spring 架构，则无法使用。该项目补充非Spring架构下的场景。


在工程下添加第三方库：
1. amqp-client-5.10.0.jar
2. logback-amqp-appender-1.0.0.jar

配置方法:

```xml
<configuration>
    <appender name="rabbitmqAppender" class="com.yiidata.logback.amqp.AmqpAppender">
        <layout>
            <pattern>{"source":"appname","dt":"%d{yyyy-MM-dd HH:mm:ss}","thread":"%thread","level":"%-5level", "logger": "%logger", "line":"%line", "msg":"%msg"}</pattern>
        </layout>
        <!--rabbitmq地址 -->
        <host>localhost</host>
        <port>5672</port>
        <username>admin</username>
        <password>123456</password>
        <exchangeType>DIRECT</exchangeType>
        <exchangeName>logback</exchangeName>
        <routingKey>testtopic</routingKey>
        <autoDelete>false</autoDelete>
    </appender>

    <root level="info">
        <appender-ref ref="rabbitmqAppender" />

    </root>
</configuration>
```
